import { Component, Input, OnInit } from '@angular/core';
import { Poll } from '../../../../core/models/poll.model';
import { Choice } from '../../../../core/models/choice.model';
import { Answer } from '../../../../core/enums/answer.enum';
import { StorageService } from '../../../../core/services/storage.service';

@Component({
	selector: 'app-choice-button-dinum',
	templateUrl: './choice-button-dinum.component.html',
	styleUrls: ['./choice-button-dinum.component.scss'],
})
export class ChoiceButtonDinumComponent implements OnInit {
	@Input() public poll: Poll;
	@Input() public choice: Choice;
	@Input() public answerKind: string = 'yes';

	public answerEnum = Answer;

	constructor(public storageService: StorageService) {}

	ngOnInit(): void {}
}
