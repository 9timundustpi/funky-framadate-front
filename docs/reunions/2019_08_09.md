# notes du 9/8/2019 avec Mumble

À discuter :

    discussion via framatalk ? plutôt mumble ! 

    Se connecter à mumble.tcit.fr. Il n'a pas de mot de passe et il y a un salon Framadate ouvert.


    qui peut/veut faire quoi ?

    tykayn: dev front end, JS & styles

    newick : intégrateur dans la vraie vie

    llaq : plutôt HTML

    talone : plutôt coté JS


    on ne s'occupe toujours que du front ?

    [newick] Est-ce que tout le monde est ok sur le fait de faire que le front et pas tout ?

    llaq : pas de souci avec le back, donc pourquoi le changer ?

    Ok on y touche pas


    repartir de 0 ?

    Je parle de funky framadate 

    llaq : Pourquoi pas recommencer car c'est bien différent des maquettes

    newick : On est toujours en mode "MVP", on fait ça pour tester les maquettes et ensuite seulement on le connecte au back-end ?

    maiwann : Ouip tant que ça reste cohérent niveau expérience de l'utilisateur

    newick : C'est plutot coté JS qu'il faut voir… talone ?

    Talone : on peut faire des choses, je maitrise pas php 

    llaq : moi non plus

    talone : il y a des contributeurices de framadate ici ?

    maiwann : nope :x

    newick : on peut avancer comme ça, surtout sur le front et ensuite on verra le back ?

    talone & llaq : oui on peut faire ça


[tentative de tykayn de nous rejoindre]

    pourquoi y'a des pages php dans le dossier front end ?

    newick : C'est dans le dossier principal, pas dans funky 

    maiwann : donc a priori rien d'obligatoire ?


    API de backend existe t elle?

    llaq : nope ^_^




    Sass, css, autre ?

    https://css2sass.herokuapp.com/ pour convertir le css actuel vers du sass




    intérêt de Bootstrap / frameworks ?

    Angular cli pour se concentrer sur des composants front end

    Boostrap a priori c'est pas forcément nécessaire, newick assez à l'aise pour ne pas en avoir besoin dans HTML / CSS

    Composants <3 Atomic design <3

    tykayn : propose de faire une démo

    (Beaucoup de discussion sur les framework front, j'ai pas tout compris ce qu'il se passait donc mes notes sont très limitées !)

    En gros, on fait du Angular et chacun intègre son truc indépendamment



Invitation sur funkyframadate, on repart de 0 !
La suite
remplissons le wiki et les board d'issues
on zieute la démo et on voit ce qu'on fait ensuite


--------
# Questions notées par Come.
Suite au visionnage de la vidéo de présentation de maiwann (Framadate_Presentation_maquettes.flv), des questions ont été levées :


* Quel est l'intérêt de différentier les dates limites de modification et vote ? Pourquoi appelle-t-on cela archivage ?
* Peut-on écrire plus explicitement que le nom du créateur, s'il est renseigné, est affiché à ceux qui répondent ?
* Lors de la validation du vote, peut-on mettre davantage en avant l'importance de l'URL d'édition afin d'éviter la fermeture machinale (par réflexe) du popup ?
* Pour la vue des réponses, pourquoi ne pas mettre pour tous les utilisateurs les petites icônes actuellement réservées aux daltoniens ? On économiserait alors un champ.
* Quitte à faire la chasse aux clics, pour les menus déroulants à deux options (du type « je veux » / « je ne veux pas ») qui nécessitent deux clics pour commuter, pourquoi pas une commutation en un clic ; comme une check-box mais avec du texte à la place du check ? Bien-sûr, ceci à condition que les champs conditionnels ne soient pas vidés lorsqu'ils disparaissent.


* Y a-t-il des champs obligatoires ? Si oui, comment les matérialiser ?
* En cas d'erreur à la validation du formulaire (pour cause de champ vide ou format incorrect par exemple), on affiche systématiquement le message d'erreur en orange (cf. écran sondage_date_intervalle) en dessous du champ concerné ?
* Quel est l'état de focus sur les champs ? Celui par défaut du navigateur ?
* Étant donné qu'il y a plusieurs étapes dans la création du sondage, ne faudrait-il pas les indiquer au début de la page ? Ex : 1/3, 2/3, 3/3 (je tire ça des bonnes pratiques opquast)
* Dans le récapitulatif à la fin, pourquoi ne rappelle-t-on pas également le titre et la description du sondage créé ?

Les boutons :

* Quel est l'état de focus sur les boutons ?

* Sur l'écran « Mot de passe », le bouton « Voir » est-il immédiatement actif ou on attend d'avoir tapé la première lettre ? Une fois qu'on a cliqué sur « Voir », est-ce que le contenu du bouton devient « Masquer » ?

* Dans ta vidéo tu précises que le popup pour ajouter l'intervalle de dates se situe en dessous du calendrier pour qu'on puisse voir les dates préalablement sélectionnées, mais je pense que sur mobile ça va être compliqué : ça signifierait que le popup se situe très bas dans l'écran, il n'y aurait que le titre visible sur les téléphones de petite taille.

* Dans les écrans de réponse au sondage date, quelle est la règle pour l'abbréviation des dates ? Des fois c'est le jour qui est abrégé, des fois le mois.

* On part sur une version desktop à partir de 768px ? Je demande, car tes maquettes desktop font 602 px de large et je ne sais pas si c'est délibéré ou non.

